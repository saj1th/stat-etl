
# STAT Data Services

### General Assumptions
The crawler job would crawl the search engines and generate csv files in S3. Orchestration / scheduling of this activity is not tightly coupled with the ETL process and is handled independently



## Goal - I / ETL

>  Design and build an ETL process to ingest and store data in an efficient format for analysis

*Assumption - I*

ETL has the responsibility to process ( basic data quality checks ) and store data in an efficient format for analysis only

For this use-case, AWS glue would be a good candidate. It has advantages in terms of cost and operational overhead.

More details at [Glue](./etl/glue/)

---
*Assumption - II*

ETL is more like a pipeline and could have some additional responsibilities like

- run complex analytics queries
- run ML algos
- persist results to multiple data stores

For this use-case, an orchestration layer of choice (a DAG engine) would be ideal. 

More details at [Airflow](./etl/airflow/)

---

## Goal - II /  Analysis    
> Analyze the data to answer queries

Spark used as a unified processing engine . 

More details at [Spark](./analysis/spark/)