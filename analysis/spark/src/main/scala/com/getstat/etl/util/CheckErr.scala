package com.getstat.etl.util

import scala.util.Try

/** Check acts as a convenience func for error check
  */
object CheckErr {

  /**
    *
    * @param t   the Try type to check
    * @param str string to print in case it was a Failure
    * @tparam T Type parameter
    * @return status whether t was a failure or not
    */
  def apply[T](t: Try[T], str: String): Boolean = {
    CheckErr(t, str, quit = true)
  }

  /**
    *
    * @param t    the Try type to check
    * @param str  string to print in case it was a Failure
    * @param quit flag to denote whether execution should halt or not
    * @tparam T Type parameter
    * @return status whether t was a failure or not
    */
  def apply[T](t: Try[T], str: String, quit: Boolean): Boolean = {
    if (t.isFailure) {
      //scalastyle:off println println(...)//
      println(str)
      println("Err: " + t.failed.get)
      //scalastyle:on println println(...)//
      if (quit) {

        System.exit(-1)
      }
    }
    t.isFailure
  }
}
