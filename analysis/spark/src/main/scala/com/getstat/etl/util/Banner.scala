package com.getstat.etl.util

import com.github.lalyos.jfiglet.FigletFont

object Banner {
  def print(text: String): Unit = {
    //scalastyle:off println println(...)//
    println(FigletFont.convertOneLine(text))
    //scalastyle:on println println(...)//

  }
}
