package com.getstat.etl.process

import com.getstat.etl.conf.Config
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object Analyze {

  def showMostRanks(df: DataFrame)(implicit conf: Config): Unit = {
    mostRanks(df).show(conf.offset)
  }

  //1. Which URL has the most ranks in the top 10 across all keywords over the period?
  def mostRanks(df: DataFrame): DataFrame = {
    val spark = SparkSession.getActiveSession.get
    import spark.implicits._

    df.filter($"rank" < 10)
      .groupBy($"url")
      .agg(count("url").alias("count"))
      .orderBy(desc("count"))
      .select("count", "url")
  }

  def showTopRankChanges(df: DataFrame)(implicit conf: Config): Unit = {
    topRankChanges(df).show(conf.offset)
  }

  //2. Provide the set of keywords (keyword information) where the rank 1 URL changes the most over
  //the period. A change, for the purpose of this question, is when a given keyword's rank 1 URL is
  //different from the previous day's URL.
  def topRankChanges(df: DataFrame): DataFrame = {
    val spark = SparkSession.getActiveSession.get
    import spark.implicits._

    val w = Window.partitionBy("keyword", "device", "market").orderBy("date")
    val windowDf =
      df.filter($"rank" === 1).withColumn("lastTopUrl", lag("url", 1).over(w))
    val deltaDf = windowDf.withColumn("delta", when($"lastTopUrl".isNull || $"lastTopUrl" === $"url", 0).otherwise(1))

    deltaDf
      .groupBy("keyword")
      .agg(sum("delta").alias("delta"))
      .orderBy(desc("delta"))
  }

  def showDeviceVariance(df: DataFrame)(implicit conf: Config): Unit = {
    deviceVariance(df).show(conf.offset)
  }

  // 3. We would like to understand how similar the results returned for the same keyword, market, and
  //  location are across devices. For the set of keywords, markets, and locations that have data for
  // both desktop and smartphone devices, please devise a measure of difference to indicate how similar
  // the URLs and ranks are, and please use this to show how the mobile and desktop results in our provided
  // data set converge or diverge over the period.
  def deviceVariance(df: DataFrame): DataFrame = {
    val spark = SparkSession.getActiveSession.get
    import spark.implicits._

    val maxRank = 101
    val desktopDf = df
      .filter($"device" === "desktop")
      .withColumnRenamed("rank", "desktopRank")
      .withColumnRenamed("device", "isDesktop")

    val mobileDf = df
      .filter($"device" === "smartphone")
      .withColumnRenamed("rank", "mobileRank")
      .withColumnRenamed("device", "isSmartPhone")

    val joinExp = Seq("keyword", "market", "url", "date")

    // Approximation of null ranks to 101
    // Assumption that if there is no rank for a particular (device, url) combination,
    // it's because the rank is above 100
    desktopDf
      .join(mobileDf, joinExp, "left")
      .withColumn("desktopRank", when($"desktopRank".isNull, maxRank).otherwise($"desktopRank"))
      .withColumn("mobileRank", when($"mobileRank".isNull, maxRank).otherwise($"mobileRank"))
      .withColumn("delta", abs($"desktopRank" - $"mobileRank"))

    /**
    *
    Q. What would be a good measure of difference to indicate how similar the URLs and ranks are
       for a particular keyword and market

      we could do a group the above dataframe by keyword and market and get agg metrics like
      avergae, mean or stddev.

      Need more details of the business case of this measure to better understand the choice of metric

      Delta itself would show how the ranks differs between mobile and desktop.
      If delta is low, results converge, if its high, they diverge

    */

  }

}
