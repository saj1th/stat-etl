package com.getstat.etl.process

import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._

object QCheck {

  // normalizes the keywords
  def normalizeKeyWords(url: String): String = {
    //handle keywords like  """analytics seo""" , """not provided"" google"", ""door ""guest contributor""" etc
    Option(url).getOrElse("").replaceAll("\"\"", "").replaceAll("\"\"\"", "")
  }

  // spark udf to return the normalized keyword
  def normalize: UserDefinedFunction = udf(normalizeKeyWords _)

  def apply(df: DataFrame): DataFrame = {
    val spark = SparkSession.getActiveSession.get
    import spark.implicits._

    val baseDf = df
      .filter(
        $"keyword".isNotNull && $"market".isNotNull &&
          $"device".isNotNull && $"date".isNotNull &&
          $"rank".isNotNull && $"url".isNotNull)
      .withColumn("keyword", normalize($"keyword"))
      .drop("location") //location seems to be null - need more clarity

    // Remove duplicates. Current dataset has multiple ranks
    // for the same "keyword", "market", "device", "date" combination
    baseDf
      .groupBy("keyword", "market", "device", "date", "rank")
      .agg(max("url").alias("url"))

    // TODO: Handle non-utf8 characters.
    // Input data seems to have issues
    // e.g serp ñ‚ñ€ñðºð¸ð½ð³

    // TODO: check the jobs and stages and see
    // if caching need to be done here instead of SerpsLoader
  }
}
