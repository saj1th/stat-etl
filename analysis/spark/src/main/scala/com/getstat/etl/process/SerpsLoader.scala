package com.getstat.etl.process

import com.getstat.etl.conf.Config
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._
import org.slf4j.LoggerFactory

object SerpsLoader {

  val schema = StructType(
    Array(
      StructField("keyword", StringType, true),
      StructField("market", StringType, true),
      StructField("location", StringType, true),
      StructField("device", StringType, true),
      StructField("date", DateType, true),
      StructField("rank", IntegerType, true),
      StructField("url", StringType, true)
    ))

  def apply()(implicit spark: SparkSession, conf: Config): DataFrame = {
    val log = LoggerFactory.getLogger(conf.name)

    val df = if (conf.isLocal) {
      log.info(s"loading data from local path:${conf.aws.localPath}")
      spark.read
        .format("csv")
        .option("header", "true")
        .schema(schema)
        .load(s"${conf.aws.localPath}")
    } else {
      log.info(s"loading data from path:${conf.aws.s3Scheme}://${conf.aws.inS3Bucket}/${conf.aws.inS3Path}")
      spark.read
        .format("csv")
        .option("header", "true")
        .schema(schema)
        .load(s"${conf.aws.s3Scheme}://${conf.aws.inS3Bucket}/${conf.aws.inS3Path}")
    }

    val inputCount = df.persist(conf.getCacheLevel).count()

    log.info(s"processing $inputCount records for job ${conf.jobID}")

    df
  }
}
