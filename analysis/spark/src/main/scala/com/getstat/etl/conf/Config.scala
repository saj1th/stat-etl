package com.getstat.etl.conf

import com.typesafe.config.ConfigFactory

import scala.util.Try
import io.circe.generic.auto._
import io.circe.config.syntax._
import org.apache.spark.storage.StorageLevel
import com.gilt.timeuuid._

case class Config(name: String, logLevel: String, mode: String, offset: Int, spark: Spark, aws: Aws) {

  val isLocal: Boolean = this.mode == "local"
  //used to identify the run based on the inPath. useful for cleanups in case of re-runs
  val runID: String = this.aws.inS3Path

  //jobId used for feeding metric.
  val jobID: String = this.runID + TimeUuid().toString

  def getCacheLevel: StorageLevel = {
    this.spark.storageLevel match {
      case "NONE" => StorageLevel.NONE
      case "DISK_ONLY" => StorageLevel.DISK_ONLY
      case "DISK_ONLY_2" => StorageLevel.DISK_ONLY_2
      case "MEMORY_ONLY" => StorageLevel.MEMORY_ONLY
      case "MEMORY_ONLY_2" => StorageLevel.MEMORY_ONLY_2
      case "MEMORY_ONLY_SER" => StorageLevel.MEMORY_ONLY_SER
      case "MEMORY_ONLY_SER_2" => StorageLevel.MEMORY_ONLY_SER_2
      case "MEMORY_AND_DISK" => StorageLevel.MEMORY_AND_DISK
      case "MEMORY_AND_DISK_2" => StorageLevel.MEMORY_AND_DISK_2
      case "MEMORY_AND_DISK_SER" => StorageLevel.MEMORY_AND_DISK_SER
      case "MEMORY_AND_DISK_SER_2" => StorageLevel.MEMORY_AND_DISK_SER_2
      case "OFF_HEAP" => StorageLevel.OFF_HEAP
      case _ => StorageLevel.MEMORY_AND_DISK
    }
  }
}

case class Secrets(mysql_report: AccessCred, etl_tracker: AccessCred)

case class AccessCred(username: String, password: String)

case class Spark(
  master: String,
  coalesce: Boolean,
  numPartitions: Integer,
  storageLevel: String
) {
  def getStorageLevel: StorageLevel = {
    this.storageLevel match {
      case "NONE" => StorageLevel.NONE
      case "DISK_ONLY" => StorageLevel.DISK_ONLY
      case "DISK_ONLY_2" => StorageLevel.DISK_ONLY_2
      case "MEMORY_ONLY" => StorageLevel.MEMORY_ONLY
      case "MEMORY_ONLY_2" => StorageLevel.MEMORY_ONLY_2
      case "MEMORY_ONLY_SER" => StorageLevel.MEMORY_ONLY_SER
      case "MEMORY_ONLY_SER_2" => StorageLevel.MEMORY_ONLY_SER_2
      case "MEMORY_AND_DISK" => StorageLevel.MEMORY_AND_DISK
      case "MEMORY_AND_DISK_2" => StorageLevel.MEMORY_AND_DISK_2
      case "MEMORY_AND_DISK_SER" => StorageLevel.MEMORY_AND_DISK_SER
      case "MEMORY_AND_DISK_SER_2" => StorageLevel.MEMORY_AND_DISK_SER_2
      case "OFF_HEAP" => StorageLevel.OFF_HEAP
      case _ => StorageLevel.MEMORY_AND_DISK
    }
  }
}

case class Aws(
  useIam: Boolean,
  iamRole: String,
  s3Scheme: String,
  inS3Bucket: String,
  inS3Path: String,
  outS3Bucket: String,
  localPath: String,
  mysqlEtlTrackerHost: String
)

object Config {

  var _conf: Option[Config] = None

  //Singleton
  def load: Try[Config] = {
    Try({
      _conf match {
        case None =>
          val config = ConfigFactory.load()
          val pconfig = config.as[Config]
          if (pconfig.isLeft) {
            //scalastyle:off println println(...)//
            println(pconfig.left.get)
            //scalastyle:on println println(...)//
          }
          _conf = Some(pconfig.right.get)
          _conf.get
        case Some(conf) => conf
      }
    })
  }

}
