package com.getstat.etl

import com.getstat.etl.conf.Config
import com.getstat.etl.util.{Banner, CheckErr}
import com.getstat.etl.generated.BuildInfo
import com.getstat.etl.process.{Analyze, ParquetRefresher, QCheck, SerpsLoader}
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

import scala.util.{Failure, Success, Try}

object App {
  def main(args: Array[String]): Unit = {
    // Load the config
    val conf = Config.load
    CheckErr(conf, "Unable to process conf\nPlease check your application.conf / env variables")

    process(conf.get)
  }

  private def process(implicit conf: Config): Unit = {

    // Get the app name and version mentioned in build.sbt
    val appName = BuildInfo.name + "-" + BuildInfo.version

    val log = LoggerFactory.getLogger(conf.name)

    log.info(s"init: $appName jobId: ${conf.jobID}")
    Banner.print(appName)

    implicit val spark = SparkSession.builder().appName(appName).master(conf.spark.master).getOrCreate()

    Try {
      val df = QCheck(SerpsLoader())

      Analyze.showMostRanks(df)
      Analyze.showTopRankChanges(df)
      Analyze.showDeviceVariance(df)

      ParquetRefresher(df)

    } match {
      case Success(_) => {
        log.info(s"done: data processing successfully completed - jobId: ${conf.jobID}")
      }
      case Failure(err) => {
        log.error(s"err: data processing failed, ${err.getCause}")
        log.error("err:", err)
        throw err
      }
    }
  }

}
