package com.getstat.etl.process

import com.getstat.etl.conf.Config
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.date_format
import org.slf4j.LoggerFactory

/**
  * persists the  data
  * to s3 in parquet format
  *
  */
object ParquetRefresher {
  def apply(df: DataFrame)(implicit spark: SparkSession, conf: Config): Unit = {
    import spark.implicits._
    val log = LoggerFactory.getLogger(conf.name)

    log.info(s"writing parquet files")

    val s3Scheme = conf.aws.s3Scheme
    val pathPrefix = s"$s3Scheme://"
    val dwS3Bucket = conf.aws.outS3Bucket

    df.withColumn("yyyy", date_format($"date", "yyyy"))
      .withColumn("mm", date_format($"date", "MM"))
      .withColumn("dd", date_format($"date", "dd"))
      .write
      .mode("append")
      .partitionBy("yyyy", "mm", "dd")
      .parquet(s"$pathPrefix$dwS3Bucket/")

  }

}
