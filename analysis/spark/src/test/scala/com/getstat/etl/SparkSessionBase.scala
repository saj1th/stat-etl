package com.getstat.etl

import com.getstat.etl.conf.Config

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.scalatest.{BeforeAndAfter, FunSuite}

/**
  * Base class for all spark test classes.
  * Provide the sparkSession and conf.
  */
class SparkSessionBase extends FunSuite with BeforeAndAfter {

  protected val conf = Config.load

  val spark: SparkSession = SparkSession.builder().appName("test").master("local").getOrCreate()

}
