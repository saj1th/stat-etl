package com.getstat.etl

// Used to run the app locally when
// spark dependencies are marked as provided

object Run {
  def main(args: Array[String]): Unit = {
    App.main(args)
  }
}
