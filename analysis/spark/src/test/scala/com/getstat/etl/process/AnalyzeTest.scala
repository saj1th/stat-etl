package com.getstat.etl.process

import com.getstat.etl.SparkSessionBase
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._

class AnalyzeTest extends SparkSessionBase {

  val schema = StructType(
    Array(
      StructField("keyword", StringType, true),
      StructField("market", StringType, true),
      StructField("location", StringType, true),
      StructField("device", StringType, true),
      StructField("date", DateType, true),
      StructField("rank", IntegerType, true),
      StructField("url", StringType, true)
    ))

  lazy val read: DataFrame = {
    spark.read
      .format("csv")
      .option("header", "true")
      .schema(schema)
      .load("src/test/resources/5daysample.csv")
  }

  test("Most Ranks") {
    val df = read
    val most = Analyze.mostRanks(QCheck(df)).collectAsList().get(0)

    assert(most.get(0) == 3)
    assert(most.get(1) == "getstat.com/")
  }

  test("Top Rank Variance") {
    val df = read
    val most = Analyze.topRankChanges(QCheck(df)).collectAsList().get(0)

    assert(most.get(0) == "search analytics")
    assert(most.get(1) == 2)
  }
}
