name := "stat-analysis"

version := "0.7"

scalaVersion := "2.11.12"
scalacOptions ++= Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-target:jvm-1.8",
  "-unchecked",
  "-Ywarn-adapted-args",
  "-Ywarn-unused",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Xlint"
)

libraryDependencies ++= {
  object V {
    val spark = "2.3.1"
    val awsSdk = "1.11.409"
    val funSuit = "2.3.1_0.10.0"
    val scalacheck = "1.14.0"
    val scalaMock = "2.4"
    val typesafeConfig = "1.3.1"
    val jfiglet = "0.0.8"
    val slf4j = "1.7.16"
    val circeConf = "0.4.1"
    val circeGeneric = "0.10.0-M2"
    val giltUUid = "0.0.8"
  }

  Seq(
    "com.amazonaws" % "aws-java-sdk" % V.awsSdk,
    "io.circe" %% "circe-config" % V.circeConf,
    "io.circe" %% "circe-generic" % V.circeGeneric,
    "com.github.lalyos" % "jfiglet" % V.jfiglet,
    "org.apache.spark" %% "spark-core" % V.spark, // % "provided",
    "org.apache.spark" %% "spark-sql" % V.spark, // % "provided",
    "com.github.lalyos" % "jfiglet" % V.jfiglet,
    "com.amazonaws" % "aws-java-sdk" % V.awsSdk,
    "com.holdenkarau" %% "spark-testing-base" % V.funSuit % "test",
    "org.scalacheck" %% "scalacheck" % V.scalacheck % "test",
    "org.scalamock" % "scalamock-compiler-plugin_2.9.0-1" % V.scalaMock,
    "com.gilt" %% "gfc-timeuuid" % V.giltUUid,
    "com.typesafe" % "config" % V.typesafeConfig,
    "org.slf4j" % "slf4j-api" % V.slf4j,
    "org.slf4j" % "slf4j-log4j12" % V.slf4j
  )
}

lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin)
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion),
    buildInfoPackage := "com.getstat.etl.generated"
  )

//For running on the local machine
fork in Test := true
javaOptions in Test ++= Seq("-Xms1512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")
parallelExecution in Test := false
fork in Test := false

assemblyOutputPath in assembly := file(s"""./${artifact.value.name}-${version.value}.jar""")

assemblyMergeStrategy in assembly := {
  case "log4j.properties" => MergeStrategy.first
  case m: Any if m.toLowerCase.endsWith("manifest.mf") => MergeStrategy.discard
  case m: Any if m.toLowerCase.matches("meta-inf.*\\.sf$") =>
    MergeStrategy.discard
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}

resolvers ++= Seq(
  "Spark Packages Repo" at "http://dl.bintray.com/spark-packages/maven",
  "Cloudera Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/",
  "Sonatype releases" at "https://oss.sonatype.org/content/repositories/releases",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "TypeSafe releases" at "http://repo.typesafe.com/typesafe/releases",
  Resolver.mavenLocal
)

updateOptions in Global := updateOptions
  .in(Global)
  .value
  .withCachedResolution(true)
