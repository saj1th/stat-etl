## Glue

The Spark Job 


 - reads data from S3  
 - performs data quality checks
 - generate results for the queries ( currently displayed at stdout, but could be persisted to a data store of choice)
 - saved data in parquet format for future analysis
