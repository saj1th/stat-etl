from airflow import DAG
from datetime import datetime, time, timedelta
from airflow.operators.sensors import TimeSensor
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.models import Variable

from etl import util


cur_dag = 'stat_etl_v01'
cur_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

email_group = Variable.get("email_group")
email_group = ""
args = {
    'owner': 'getstat',
    'start_date': cur_date,
    'email': [email_group],
    'email_on_failure': True,
    'email_on_retry': True,
    'depends_on_past': True
}
config = Variable.get("etl_config", deserialize_json=True)


dag = DAG(
    dag_id=cur_dag, default_args=args,
    schedule_interval='0 0 * * *',
    dagrun_timeout=timedelta(hours=10))

# Tasks

start_dag = BashOperator(
    task_id='start_dag',
    bash_command='echo starting stat etl',
    dag=dag)

create_emr = PythonOperator(
    task_id='create_emr',
    provide_context=True,
    python_callable=util.create_emr,
    params=config,
    dag=dag)

add_emr_steps = PythonOperator(
    task_id='add_emr_steps',
    provide_context=True,
    python_callable=util.add_emr_steps,
    params=config,
    dag=dag)

terminate_emr = PythonOperator(
    task_id='terminate_emr',
    provide_context=True,
    python_callable=util.terminate_emr_cluster,
    dag=dag)

# TODO: Add more tasks here as needed

create_emr.set_upstream(start_dag)
add_emr_steps.set_upstream(create_emr)
terminate_emr.set_upstream(add_emr_steps)
