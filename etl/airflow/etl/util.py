# from datetime import timedelta
import time
import datetime as DT
import logging
import boto3
import json
import requests
import paramiko
from airflow.models import Variable
from requests_aws4auth import AWS4Auth

session = boto3.Session()
credentials = session.get_credentials()
region = 'us-west-2'
awsauth = AWS4Auth(credentials.access_key, credentials.secret_key, region, 'es', session_token=credentials.token)


def create_emr(**kwargs):
    emrclient = boto3.client('emr', region_name=region)

    # This logic is needed because when there are no spot instances requested, LaunchSpecifications cannot be used
    if kwargs['params']['slave_spot_capacity'] > 0:
        core_instance_specifications = {
                    'Name': 'CORE',
                    'InstanceFleetType': 'CORE',
                    'TargetOnDemandCapacity': kwargs['params']['slave_ondemand_capacity'],
                    'TargetSpotCapacity': kwargs['params']['slave_spot_capacity'],
                    'InstanceTypeConfigs': [
                        {
                            'InstanceType': kwargs['params']['slave_type'],
                            'BidPriceAsPercentageOfOnDemandPrice': kwargs['params']['slave_bid_percentage'],
                            'WeightedCapacity': 1,
                            'EbsConfiguration': {
                                'EbsBlockDeviceConfigs': [
                                    {
                                        'VolumeSpecification': {
                                                'SizeInGB': kwargs['params']['emr_ebs_size'],
                                                'VolumeType': 'gp2'
                                        },
                                        'VolumesPerInstance':1
                                    }
                                ]
                            },
                            'Configurations': []
                        },
                    ],
                    'LaunchSpecifications': {
                        'SpotSpecification': {
                            'TimeoutDurationMinutes': 5,
                            'TimeoutAction': 'SWITCH_TO_ON_DEMAND'
                        }
                    }
                }
    else:
        core_instance_specifications = {
                        'Name': 'CORE',
                        'InstanceFleetType': 'CORE',
                        'TargetOnDemandCapacity': kwargs['params']['slave_ondemand_capacity'],
                        'TargetSpotCapacity': kwargs['params']['slave_spot_capacity'],
                        'InstanceTypeConfigs': [
                            {
                                'InstanceType': kwargs['params']['slave_type'],
                                'BidPriceAsPercentageOfOnDemandPrice': kwargs['params']['slave_bid_percentage'],
                                'WeightedCapacity': 1,
                                'EbsConfiguration': {
                                    'EbsBlockDeviceConfigs': [
                                        {
                                            'VolumeSpecification': {
                                                    'SizeInGB': kwargs['params']['emr_ebs_size'],
                                                    'VolumeType': 'gp2'
                                            },
                                            'VolumesPerInstance':1
                                        }
                                    ]
                                },
                                'Configurations': []
                            },
                        ]
                    }


    response = emrclient.run_job_flow(
        Name=kwargs['params']['cluster_name'],
        LogUri=kwargs['params']['log_uri'],
        ReleaseLabel=kwargs['params']['emr_release'],
        Instances={
            'InstanceFleets': [
                {
                    'Name': 'MASTER',
                    'InstanceFleetType': 'MASTER',
                    'TargetOnDemandCapacity': 1,
                    'InstanceTypeConfigs': [
                        {
                            'InstanceType': kwargs['params']['master_type'],
                            'WeightedCapacity': 1,
                            'EbsConfiguration': {
                                'EbsBlockDeviceConfigs': [
                                    {
                                        'VolumeSpecification': {
                                                'SizeInGB': kwargs['params']['emr_ebs_size'],
                                                'VolumeType': 'gp2'
                                        },
                                        'VolumesPerInstance':1
                                    }
                                ]
                            },
                            'Configurations': []
                        },
                    ]
                },
                core_instance_specifications,
            ],
            'KeepJobFlowAliveWhenNoSteps': True,
            'TerminationProtected': False,
            'Ec2KeyName': kwargs['params']['emr_key'],
            'Ec2SubnetIds': [
                kwargs['params']['subnet'][0],
                kwargs['params']['subnet'][1],
                kwargs['params']['subnet'][2]
            ],
            'EmrManagedMasterSecurityGroup': kwargs['params']['security_group_master'],
            'EmrManagedSlaveSecurityGroup': kwargs['params']['security_group_slave']
        },
        BootstrapActions=[
            {
                'Name': 'Access BQ Cred File',
                'ScriptBootstrapAction': {
                    'Path': "s3://{0}/emr/stat-etl/bootstrap_scripts/{1}".format(kwargs['params']['config_bucket'],
                                                                                   kwargs['params']['bootstrap_script']),
                    'Args': []
                }
            },
        ],
        Applications=[
            {
                'Name': 'Hadoop'
            },
            {
                'Name': 'Spark'
            },
            {
                'Name': 'Ganglia'
            },
        ],
        Configurations=[
            {
                "Classification": "yarn-site",
                "Properties": {
                    "yarn.nodemanager.pmem-check-enabled": "false",
                    "yarn.resourcemanager.am.max-attempts": kwargs['params']['max_app_attempts']
                },
                "Configurations": []
            },
            {
                "Classification": "emrfs-site",
                "Properties": {
                    "fs.s3.consistent": "true",
                    "fs.s3.consistent.retryCount": "5",
                    "fs.s3.consistent.retryPeriodSeconds": "10",
                    "fs.s3.consistent.metadata.tableName": kwargs['params']['emrfs_metadata_table'],
                    "fs.s3.consistent.metadata.read.capacity": kwargs['params']['emrfs_metadata_read_tp'],
                    "fs.s3.consistent.metadata.write.capacity": kwargs['params']['emrfs_metadata_write_tp']
                }
            }
        ],
        VisibleToAllUsers=True,
        JobFlowRole=kwargs['params']['ec2_role'],
        ServiceRole=kwargs['params']['service_role'],
        Tags=[
            {
                'Key': 'PROJECT',
                'Value': kwargs['params']['cluster_tag']
            },
        ],
    )
    logging.info('EMR Cluster id: ' + response['JobFlowId'])
    return response['JobFlowId']

def add_emr_steps(**kwargs):
    emrclient = boto3.client('emr', region_name='eu-west-1')
    ti = kwargs['ti']
    jobflow_id = ti.xcom_pull(task_ids='create_emr')
    
    today = DT.date.today()
    response = emrclient.add_job_flow_steps(
        JobFlowId=jobflow_id,
        Steps=[
            {
                'Name': 'Stat ETL',
                'ActionOnFailure': 'CONTINUE',
                'HadoopJarStep': {
                    'Properties': [],
                    'Jar': 'command-runner.jar',
                    'Args': [
                        "spark-submit",
                        "--deploy-mode", "cluster", "--verbose",
                        "--master", "yarn",
                        "--num-executors", "{}".format(kwargs['params']['spark_num_executors']),
                        "--driver-memory", "{}".format(kwargs['params']['spark_driver_memory']),
                        "--executor-memory", "{}".format(kwargs['params']['spark_executor_memory']),
                        "--conf", "spark.yarn.driver.memoryOverhead={}".format(kwargs['params']['driver_overhead']),
                        "--conf", "spark.yarn.executor.memoryOverhead={}".format(kwargs['params']['executor_overhead']),
                        "--conf",
                        "spark.yarn.app.mapreduce.am.resource.mb={}".format(kwargs['params']['spark_am_resource_mb']),
                        "--conf", "spark.streaming.backpressure.enabled=true",
                        "--conf", "spark.driver.extraJavaOptions=-DSTAT_ETL_MODE=prod -DSTAT_ETL_SPARK_MASTER=yarn"
                                  + " -DSTAT_ETL_DEBUG_WRITE=" + kwargs['params']['debug_write']
                                  + " -DSTAT_ETL_STREAM_CHECKPOINT_LOC=" + kwargs['params']['checkpoint_loc'] 
                                  + " -DSTAT_ETL_S3_BUCKET=" + kwargs['params']['s3_bucket'],
                        "--conf",
                                  "spark.executor.extraJavaOptions=-DSTAT_ETL_MODE=prod -DSTAT_ETL_SPARK_MASTER=yarn"
                                  + " -DSTAT_ETL_DEBUG_WRITE=" + kwargs['params']['debug_write']
                                  + " -DSTAT_ETL_STREAM_CHECKPOINT_LOC=" + kwargs['params']['checkpoint_loc'] 
                                  + " -DSTAT_ETL_S3_BUCKET=" + kwargs['params']['s3_bucket'],
                        "--class", "com.getsttat.etl.Run", "{}".format(kwargs['params']['statetl_jar'])
                    ]
                }
            },
        ]
    )

    logging.info('Following steps added to the EMR Cluster:')
    logging.info(response['StepIds'])
    return response['StepIds']

def terminate_emr_cluster(**kwargs):
    ti = kwargs['ti']
    cluster_id = ti.xcom_pull(task_ids='create_emr')
    emrclient = boto3.client('emr', region_name='eu-west-1')

    msg = "End-of-day. Terminating Cluster => " + cluster_id
    logging.info(msg)
    emrclient.terminate_job_flows(JobFlowIds=[cluster_id, ])
    return msg
