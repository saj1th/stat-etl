## Airflow


The airflow dag would run daily (based on the schedule), creates an EMR cluster, runs the jar at  [Spark Job](../../analysis/spark/) and terminates the cluster


To install the required dependencies to run locally, use

https://bitbucket.org/snippets/saj1th/yebAxB  or https://github.com/puckel/docker-airflow


For production, however there would be additional steps like 

 - change the executor type ( depending on the need to scale the
   pipeline)
 - postgres failover mechanism
 - how to store and send credentials etc

