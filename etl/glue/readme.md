## Glue

The glue crawler + job converts the csv files to parquet format and makes it available for future analysis via spark, athena or presto.


**S3 folder structure**

It would be great if we could change the s3 folder structure where the crawler output is stored to https://stat-ds-test.s3.amazonaws.com/year=2017/month=07/day=01/getstat_com_serp_report.csv.gz​
This way additional metadata is available for programs like glue crawlers.


**Populating the AWS Glue Data Catalog**

More details at https://docs.aws.amazon.com/glue/latest/dg/populate-data-catalog.html

 1. Create a database
 2. Populate metadata in the tables by adding a crawler. More details at https://docs.aws.amazon.com/glue/latest/dg/add-crawler.html
 3. Add a job with the script mentioned here [Glue Script](./glue.scala)
 4. Setup S3 event notification for https://stat-ds-test.s3.amazonaws.com/ and run a lambda function that calls the crawler. https://github.com/serverless/serverless could come handy for creating lambda functions
 5. Create a CloudWatch event, Glue Crawler state change as Event source, choose a Lambda function as Event target and create a lambda function that triggers the job
